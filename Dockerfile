FROM python:3.9-slim

ARG DEBUG

WORKDIR /app

COPY . .
# Install dependencies (skipping root warning)
RUN pip install --root-user-action=ignore --no-cache-dir -r requirements.txt

# Run tests
RUN python3 -m pytest -v

# Set Flask-specific environment variables
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
# Set to 1 only if DEBUG is set and not null (otherwise set to null)
ENV FLASK_DEBUG=${DEBUG:+1}

EXPOSE 5000

CMD ["flask", "run"]
