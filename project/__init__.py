import os
from apiflask import APIFlask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

# Application and OpenAPI setup
app = APIFlask(__name__)
if not app.debug:
    app.enable_openapi = False

# Setup forms
app.config["SECRET_KEY"] = os.urandom(32)
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Database Setup
basedir = os.path.abspath(os.path.dirname(__file__))
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
    basedir, "data.sqlite"
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Setup database
db = SQLAlchemy(app)
Migrate(app, db)

# Setup JWT auth
from project.config.jwt import setup_jwt

jwt = setup_jwt(app)

# Register Blueprints
from project.core.views import core
from project.books.views import books
from project.customers.views import customers
from project.loans.views import loans

app.register_blueprint(core)
app.register_blueprint(books)
app.register_blueprint(customers)
app.register_blueprint(loans)

# Add test account
from project.core.models import User

if app.debug:
    with app.app_context():
        try:
            test_user = User.query.filter_by(username='test').first()
            if test_user is None:
                test_user = User(username='test', password='test')
                db.session.add(test_user)
            else:
                test_user.password = 'test'
            db.session.commit()
            print('Test user configured successfully')
        except Exception as e:
            db.session.rollback()
            print('Error configuring test user:', e)
