from typing import Optional

from apiflask import APIBlueprint
from flask import jsonify, render_template
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity
from project import db
from project.core.models import User, CreateUserSchema, LogInSchema


# Blueprint for core
core = APIBlueprint('core', __name__, template_folder='templates', static_folder='static')


# Route to homepage
@core.route('/')
def index():
    print('Homepage accessed')
    return render_template('index.html')


@core.route("/login", methods=["POST"])
@core.doc(responses=[200, 401])
@core.input(LogInSchema)
def login(json_data: LogInSchema):
    username = json_data.get("username", None)
    password = json_data.get("password", None)
    if username is None or password is None:
        return jsonify({"msg": "Missing username or password"}), 401
    print(User.query.all())
    user: Optional[User] = User.query.filter_by(username=username).first()
    if user is None or user.password != password:
        return jsonify({"msg": "Bad username or password"}), 401

    token = create_access_token(identity=username)
    return jsonify(token=token)


@core.route("/register", methods=["POST"])
@core.doc(responses=[200, 401, 500])
@core.input(CreateUserSchema)
def register(json_data: CreateUserSchema):
    username = json_data.get("username", None)
    password = json_data.get("password", None)
    if username is None or password is None:
        return jsonify({"msg": "Missing username or password"}), 401
    try:
        new_user = User(username=username, password=password)
        db.session.add(new_user)
        db.session.commit()
        print('User added successfully')
    except Exception as e:
        db.session.rollback()
        print('Error creating user')
        return jsonify({'error': f'Error creating user: {str(e)}'}), 500

    token = create_access_token(identity=username)
    return jsonify(token=token)


@core.route("/protected", methods=["GET"])
@core.doc(security="HTTPBearerAuth", responses=[200, 401])
@jwt_required()
def protected():
    current_user = get_jwt_identity()
    return jsonify(username=current_user), 200
