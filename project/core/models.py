import re

from apiflask import Schema
from apiflask.fields import String
from sqlalchemy.orm import validates
from project import db


# Book model
class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password = db.Column(db.String(64))

    username_pattern = re.compile('^[a-z0-9_-]{1,64}$', re.IGNORECASE)

    def __init__(self, username, password):
        self.username = username
        self.password = password

    @validates('username')
    def validate_username(self, _, username):
        if not isinstance(username, str):
            raise ValueError('Expected username to be of type str')
        username = str(username)
        (min_len, max_len) = (1, 64)
        if not min_len <= len(username) <= max_len:
            raise ValueError(f'Expected username to have length between {min_len} and {max_len}, but it has length equal to {len(username)} instead')
        if re.fullmatch(self.username_pattern, username) is None:
            raise ValueError(f'Provided username value does not match pattern {self.username_pattern}')
        return username


class CreateUserSchema(Schema):
    username = String(metadata={'description': 'User name.'})
    password = String(metadata={'description': 'User password.'})


class LogInSchema(Schema):
    username = String(metadata={'description': 'User name.'})
    password = String(metadata={'description': 'User password.'})
