import os

from flask import Flask
from flask_jwt_extended import JWTManager


def setup_jwt(app: Flask) -> JWTManager:
    app.config["JWT_SECRET_KEY"] = os.urandom(32)
    app.config["JWT_DECODE_ALGORITHMS"] = ["HS256"]
    app.config["SECURITY_SCHEMES"] = {
        "HTTPBearerAuth": {
            "type": "http",
            "scheme": "bearer",
            "bearerFormat": "JWT",
        }
    }
    return JWTManager(app)
